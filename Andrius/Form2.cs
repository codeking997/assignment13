﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Data.SqlClient;

namespace Andrius
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        /*this button allows the user to insert a new character
         * it calls the update method which takes the table as parameters
         * it then calls the fill method which also takes the table as a parameter
         * 
         */
        private void button1_Click(object sender, EventArgs e)
        {
            /*string str;


            using (SqlConnection connection = new SqlConnection("localhost\SQLEXPRESS"))
            {
                 
             }*/

            this.charactersTableAdapter.Update(this.myFirstDBDataSet1.Characters);

            this.charactersTableAdapter.Fill(this.myFirstDBDataSet1.Characters);
            //this.characterTableAdapter.Update(this.MyFirstDB.dbo.Characters);
            






        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'myFirstDBDataSet1.Characters' table. You can move, or remove it, as needed.
            this.charactersTableAdapter.Fill(this.myFirstDBDataSet1.Characters);
            // TODO: This line of code loads data into the 'myFirstDBDataSet.Character' table. You can move, or remove it, as needed.
            this.charactersTableAdapter.Fill(this.myFirstDBDataSet1.Characters);

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick_2(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        /*
         * this table allows the user to update the table, the down side is that it is hardcoded
         * it creates a SqlConnectionStringBuilder it calls the data source which is the server
         * it calls the name of the folder and sets security to true
         * then it creates a new Sql connection, it opens the connection
         * the string uses an sql query to update name where the id is the provided id
         * in the using statement it updates the name of the character
         * to Fredrik where the id is one. Then to see that it works there is a message box
         * that gives a message once the button has been pressed. 
         */
        private void updateTable_Click(object sender, EventArgs e)
        {
            string str;

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7585\\SQLEXPRESS";
            builder.InitialCatalog = "MyFirstDB";
            builder.IntegratedSecurity = true;

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                
                string sql = "UPDATE Characters SET name = @name WHERE ID = @id";
                
                using (SqlCommand cl=new SqlCommand(sql, connection))
                {
                    cl.Parameters.AddWithValue("@name", "Fredrik");
                    cl.Parameters.AddWithValue("@id", 1);
                    
                    cl.ExecuteNonQuery();
                    MessageBox.Show("updating");
                    
                }
                
            }
            
        }
        /*
         * this method works the same as the one above, the down side is that the deleting is hard coded
         * It deletes in the characters table on matching ID
         * 
         */
        private void Delete_Click(object sender, EventArgs e)
        {
            string str;

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7585\\SQLEXPRESS";
            builder.InitialCatalog = "MyFirstDB";
            builder.IntegratedSecurity = true;

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                //MessageBox.Show("Connected");
                string sql = "DELETE FROM Characters WHERE ID = @id";
                //string sql = "UPDATE Characters SET name=@name";
                using (SqlCommand cl = new SqlCommand(sql, connection))
                {
                    //cl.Parameters.AddWithValue("@name", "Sarah");
                    cl.Parameters.AddWithValue("@id", 4);

                    cl.ExecuteNonQuery();
                    MessageBox.Show("deleting");

                }
            }
            }
    }
}
