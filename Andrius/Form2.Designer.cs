﻿namespace Andrius
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Andrius.DataSet1();
            this.btnCreateDatabase = new System.Windows.Forms.Button();
            this.myFirstDBDataSet = new Andrius.MyFirstDBDataSet();
            this.characterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.characterTableAdapter = new Andrius.MyFirstDBDataSetTableAdapters.CharacterTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.myFirstDBDataSet1 = new Andrius.MyFirstDBDataSet1();
            this.charactersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.charactersTableAdapter = new Andrius.MyFirstDBDataSet1TableAdapters.CharactersTableAdapter();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.healthpointsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.manaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.armorRatingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updateTable = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myFirstDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.characterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myFirstDBDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.charactersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataSet1BindingSource
            // 
            this.dataSet1BindingSource.DataSource = this.dataSet1;
            this.dataSet1BindingSource.Position = 0;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnCreateDatabase
            // 
            this.btnCreateDatabase.Location = new System.Drawing.Point(439, 276);
            this.btnCreateDatabase.Name = "btnCreateDatabase";
            this.btnCreateDatabase.Size = new System.Drawing.Size(179, 38);
            this.btnCreateDatabase.TabIndex = 1;
            this.btnCreateDatabase.Text = "Insert";
            this.btnCreateDatabase.UseVisualStyleBackColor = true;
            this.btnCreateDatabase.Click += new System.EventHandler(this.button1_Click);
            // 
            // myFirstDBDataSet
            // 
            this.myFirstDBDataSet.DataSetName = "MyFirstDBDataSet";
            this.myFirstDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // characterBindingSource
            // 
            this.characterBindingSource.DataMember = "Character";
            this.characterBindingSource.DataSource = this.myFirstDBDataSet;
            // 
            // characterTableAdapter
            // 
            this.characterTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.healthpointsDataGridViewTextBoxColumn,
            this.manaDataGridViewTextBoxColumn,
            this.armorRatingDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.charactersBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(45, 24);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(760, 169);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick_2);
            // 
            // myFirstDBDataSet1
            // 
            this.myFirstDBDataSet1.DataSetName = "MyFirstDBDataSet1";
            this.myFirstDBDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // charactersBindingSource
            // 
            this.charactersBindingSource.DataMember = "Characters";
            this.charactersBindingSource.DataSource = this.myFirstDBDataSet1;
            // 
            // charactersTableAdapter
            // 
            this.charactersTableAdapter.ClearBeforeFill = true;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.Width = 150;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.Width = 150;
            // 
            // healthpointsDataGridViewTextBoxColumn
            // 
            this.healthpointsDataGridViewTextBoxColumn.DataPropertyName = "Healthpoints";
            this.healthpointsDataGridViewTextBoxColumn.HeaderText = "Healthpoints";
            this.healthpointsDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.healthpointsDataGridViewTextBoxColumn.Name = "healthpointsDataGridViewTextBoxColumn";
            this.healthpointsDataGridViewTextBoxColumn.Width = 150;
            // 
            // manaDataGridViewTextBoxColumn
            // 
            this.manaDataGridViewTextBoxColumn.DataPropertyName = "mana";
            this.manaDataGridViewTextBoxColumn.HeaderText = "mana";
            this.manaDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.manaDataGridViewTextBoxColumn.Name = "manaDataGridViewTextBoxColumn";
            this.manaDataGridViewTextBoxColumn.Width = 150;
            // 
            // armorRatingDataGridViewTextBoxColumn
            // 
            this.armorRatingDataGridViewTextBoxColumn.DataPropertyName = "armorRating";
            this.armorRatingDataGridViewTextBoxColumn.HeaderText = "armorRating";
            this.armorRatingDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.armorRatingDataGridViewTextBoxColumn.Name = "armorRatingDataGridViewTextBoxColumn";
            this.armorRatingDataGridViewTextBoxColumn.Width = 150;
            // 
            // updateTable
            // 
            this.updateTable.Location = new System.Drawing.Point(222, 276);
            this.updateTable.Name = "updateTable";
            this.updateTable.Size = new System.Drawing.Size(175, 37);
            this.updateTable.TabIndex = 3;
            this.updateTable.Text = "update Table";
            this.updateTable.UseVisualStyleBackColor = true;
            this.updateTable.Click += new System.EventHandler(this.updateTable_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(446, 219);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(189, 32);
            this.Delete.TabIndex = 4;
            this.Delete.Text = "delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.updateTable);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnCreateDatabase);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myFirstDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.characterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myFirstDBDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.charactersBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource dataSet1BindingSource;
        private DataSet1 dataSet1;
        private System.Windows.Forms.Button btnCreateDatabase;
        private MyFirstDBDataSet myFirstDBDataSet;
        private System.Windows.Forms.BindingSource characterBindingSource;
        private MyFirstDBDataSetTableAdapters.CharacterTableAdapter characterTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private MyFirstDBDataSet1 myFirstDBDataSet1;
        private System.Windows.Forms.BindingSource charactersBindingSource;
        private MyFirstDBDataSet1TableAdapters.CharactersTableAdapter charactersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn healthpointsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn manaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn armorRatingDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button updateTable;
        private System.Windows.Forms.Button Delete;
    }
}